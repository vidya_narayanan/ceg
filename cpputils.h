#ifndef UTILS_H
#define UTILS_H
#include<cstdlib>
#include<cmath>
#define _USE_MATH_DEFINES

double radian_to_degree(double angle);
double unif_rand();

#endif // UTILS_H
