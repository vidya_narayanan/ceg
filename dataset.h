#ifndef DATASET_H
#define DATASET_H
#include "vtkinc.h"
#include "vtkutils.h"
#include <vector>
#include <set>
#include <deque>
#include <queue>
#include <algorithm>
#include <iostream>
#include <ostream>
#include<sstream>
#include <math.h>
#include <cfloat>
#include <map>
#include "ostream"
#include <sstream>

#define MAX_PERS 2
using namespace std;

typedef std::pair< std::pair<int,int>,std::pair<int,int> > _PAIR;
#define _MAKE_PAIR(x,y,z,w) \
    std::make_pair(std::make_pair(x,y),std::make_pair(std::min(z,w),std::max(z,w))) \

template < class data_type >
class point
{
private:
    data_type x; data_type y; data_type z;
public:
    point(){}

    void set(data_type i, data_type j, data_type k)
    {
        x = i; y = j; z = k;
    }
    data_type* get3()
    {
        data_type *p = new double[3];
        p[0] = x; p[1] = y; p[2] = z;
        return p;
    }
    data_type getx(){return x;}
    data_type gety(){return y;}
    data_type getz(){return z;}
    data_type dist(point<data_type> p)
    {
        data_type xval = this->x - p.getx();
        data_type yval = this->y - p.gety();
        data_type zval = this->z - p.getz();
        return sqrt(xval*xval + yval*yval + zval*zval);
    }
    data_type len()
    {
        return sqrt(this->x*this->x + this->y*this->y + this->z*this->z);
    }
    void normalize()
    {
        data_type length = len();
        if(length > 0)
        {
            this->x = this->x / length;
            this->y = this->y / length;
            this->z = this->z / length;
        }
    }

};


class edgeinfo{
public:
    int i;
    int j;
    int s;
    double cost;
    double delete_cost;
    double local_cost;
    double i_val;
    double j_val;
    double s_val;
    double distance;
    double hyper_distance;
    double priority;
    edgeinfo(){
        cost = 0;
        distance = 0;
        hyper_distance = 0;
    }
    edgeinfo(int ii, int jj)
    {
        i = ii; j = jj;
        cost = 0;
        distance = 0;
        hyper_distance = 0;

    }
    void set(int ii, int jj, int ss, double iv, double jv, double sv)
    {
        i = ii; j = jj; s = ss; i_val = iv; j_val = jv; s_val = sv;
    }
    double check_cost(edgeinfo e, bool use_penalty=false)
    {
        // 1. check that there is one common node
        // 2. cost of cancelling common node with saddle on either side
        // 3. check if cancelled agains lower maxima
        // 4. cost of updating maxima
        // 5. return min_cost + update_cost
        if(this->cost == DBL_MAX || e.cost == DBL_MAX)
            return DBL_MAX;
        double a_val,b_val,c_val;
        assert(this->i != this->j);
        assert(e.i != e.j);
        if(e.i == this->i || e.i == this->j )
        {
            a_val = e.j_val;
            b_val = ( e.i == this->i) ? max(e.i_val,this->i_val): max(e.i_val,this->j_val);
            c_val = (this->i==e.i) ? this->j_val : this->i_val;
        }
        else if(e.j == this->j || e.j == this->i)
        {


            a_val = e.i_val;
            b_val = (e.j==this->j) ? max(e.j_val,this->j_val) : max(e.j_val,this->i_val);
            c_val = (this->j==e.j) ? this->i_val : this->j_val;
        }
        else
        {
            assert(0);
        }


        double existing_cost = max(this->local_cost,e.local_cost);
        double s1 = this->s_val;
        double s2 = e.s_val;
        double penalty = 0;
        double actual_cost = 0;
        if(fabs(b_val-s1) < fabs(b_val-s2))
        {
            penalty = max(0.0,b_val-c_val);
            actual_cost = fabs(b_val-s1);
        }
        else
        {
            penalty = max(0.0,b_val-a_val);
            actual_cost = fabs(b_val-s2);
        }

        assert(actual_cost >= 0 && penalty >= 0);
        //assert( (actual_cost + penalty) <= 1);
        if(penalty > 0 && !use_penalty)
            actual_cost = 1;
        actual_cost += penalty;
        actual_cost = min(actual_cost,1.0);
        return max(existing_cost,actual_cost);



    }
    edgeinfo new_edge(edgeinfo e, bool use_penalty = false)
    {
        // generate a new edge from this.i -> e.j
        edgeinfo n;
        int a,c;
        double a_val,b_val,c_val;
        assert(this->i != this->j);
        assert(e.i != e.j);
        if(e.i == this->i || e.i == this->j )
        {

            a = e.j;
            c = (this->i==e.i) ? this->j : this->i;
            a_val = e.j_val;
            b_val = ( e.i == this->i) ? max(e.i_val,this->i_val): max(e.i_val,this->j_val);
            c_val = (this->i==e.i) ? this->j_val : this->i_val;
        }
        else if(e.j == this->j || e.j == this->i)
        {

            a = e.i;
            c = (this->j==e.j) ? this->i : this->j;

            a_val = e.i_val;
            b_val = (e.j==this->j) ? max(e.j_val,this->j_val) : max(e.j_val,this->i_val);
            c_val = (this->j==e.j) ? this->i_val : this->j_val;

        }
        else
        {
            assert(0);
        }

        double s1 = this->s_val;
        double s2 = e.s_val;
        double penalty = 0;
        double actual_cost = 0;
        if(fabs(b_val-s1) < fabs(b_val-s2))
        {

            actual_cost = fabs(b_val-s1);
            penalty = max(0.0,b_val-c_val);
            if(penalty > 0 && use_penalty)
                c_val = b_val;
        }
        else
        {

            actual_cost = fabs(b_val-s2);
            penalty = max(0.0,b_val-a_val);
            if(penalty > 0 && use_penalty)
                a_val = b_val;
        }


        double existing_cost = max(this->local_cost,e.local_cost);
        n.i = min( a,c);
        n.j = max(a,c);
        if(penalty > 0 && !use_penalty)
            actual_cost = 1;

        n.local_cost = max(actual_cost,existing_cost);
        actual_cost+=penalty;
        actual_cost = min(actual_cost,1.0);
        n.cost  =  max( actual_cost,existing_cost);
        n.i_val = n.i == a ? a_val : c_val;
        n.j_val = n.j == c ? c_val : a_val;
        n.s_val = min(this->s_val,e.s_val);
        n.s = this->s_val < e.s_val ? this->s : e.s;
        n.distance= this->distance + e.distance;
        n.hyper_distance = this->hyper_distance + e.hyper_distance;

        return n;
    }

};

typedef struct {
    int id;
    double value;
}order;

struct order_greater{

    bool operator()(order a, order b){ return a.value==b.value ? a.id > b.id : a.value > b.value; }

};
struct order_lesser{

    bool operator()(order a, order b) {return a.value==b.value ? a.id < b.id : a.value < b.value; }

};


class moments {
public:
    moments(){

        m000 = 0;
        m100 = 0;
        m010 = 0;
        m001 = 0;
        m002 = 0;
        m020 = 0;
        m200 = 0;
        m011 = 0;
        m101 = 0;
        m110 = 0;
        c000 = 0;
        c100 = 0;
        c010 = 0;
        c001 = 0;
        c002 = 0;
        c020 = 0;
        c200 = 0;
        c011 = 0;
        c101 = 0;
        c110 = 0;

    }
    double m000;
    double m100;
    double m010;
    double m001;
    double m002;
    double m020;
    double m200;
    double m011;
    double m101;
    double m110;
    double c000;
    double c100;
    double c010;
    double c001;
    double c002;
    double c020;
    double c200;
    double c011;
    double c101;
    double c110;
    //double m111;
    double phi1()
    {
        return ((c200 + c020 + c002)/*/pow(c000,5.0/3.0)*/);
    }
    double phi2()
    {
        return (c200*c200 + c020*c020 + c002*c002 + 2*c110*c110 + 2*c101*c101 + 2*c011*c011)/*/(pow(c000,10.0/3.0))*/;
    }
    double phi3()
    {
        return (c200*c200*c200 + 3*c200*c110*c110 + 3*c200*c101*c101 + 3*c110*c110*c020 + 3*c101*c101*c002 + c020*c020*c020 +
                3*c020*c011*c011 + 3*c011*c011*c002 + c002*c002*c002 + 6*c110*c101*c011)/*/(pow(c000,5.0))*/;
    }

    moments operator+(const moments& rhs)
    {
        moments lhs = *this;
        lhs.m000 += rhs.m000;
        lhs.m002 += rhs.m002;
        lhs.m011 += rhs.m011;
        lhs.m020 += rhs.m020;
        lhs.m101 += rhs.m101;
        lhs.m110 += rhs.m110;
        lhs.m200 += rhs.m200;
        lhs.m100 += rhs.m100;
        lhs.m010 += rhs.m010;
        lhs.m001 += rhs.m001;
        return lhs;
    }

    void makeCentral()
    {
        assert(m000 > 0);
        c000 = m000;
        c100 = m100/m000;
        c010 = m010/m000;
        c001 = m001/m000;

        c200 = m200 - m100*c100;
        c020 = m020 - m010*c010;
        c002 = m002 - m001*c001;

        c110 = m110 - c100*m100 - c010*m010 + c100*c010*m000;
        c011 = m011 - c010*m010 - c001*m001 + c001*c010*m000;
        c101 = m101 - c100*m100 - c001*m001 + c100*c001*m000;
    }



};
class pPair{
public:
    pPair(){value = -1;sur_ext = -1;}
    // pPair(int s, int e);
    pPair(int s, int e, int ee=-1, bool area_wise = false);

    int sad;
    int ext;
    int sur_ext;

    pair<int,int> getPair(){ return make_pair(sad,ext);}
    double value;

};
struct pPair_less{
    bool operator()(pPair a, pPair b) {return a.value < b.value; }

};
struct pPair_greater{
    bool operator()(pPair a, pPair b) {return (a.value == b.value )? (a.ext == b.ext ? a.sad > b.sad : a.ext > b.ext) : a.value > b.value; }

};
typedef map< pair<int,int>, pair< pair<int,double>, pair<double,double> > > SSP_MAP;

class dataset // Singleton
{

public:
    static dataset& getInstance()
    {
        static dataset instance;
        return instance;
    }

    friend class pPair;
    std::map<pair<int,int>, double> costs;
    std::map<pair<int,int>, int> arc_saddles;
    std::map<pair<int,int>,int> sads;
    std::map<pair<int,int>,int> intermediate;

    std::map<int, std::set<int> >candidateSets;
    std::map<int, std::set<int> >children;
    std::map<int, std::set<int> >neighbours;

    std::set<int> vsaddles;
    std::map<int, double > regionSize;
    std::map< std::pair<int,int> , double > AdjacencyStrength;

    std::map<int, moments> RegionMoments;
    std::map<pair<int,int>,int> lowestSaddle;
    SSP_MAP ssp_min;
    SSP_MAP ssptemp;

    priority_queue< pPair, vector<pPair>, pPair_greater > pqPers;
    int most_persistent_saddle;

    bool invert;
    double simUpto;
    double boundaryFraction;
    std::set<int> extremaSets;

    std::map< int, std::set<int> > lowerRingSets;
    std::map< int, std::set<int> > labelListSets;
    std::map< int, int>            highestNbr;

    std::map< int, int> persistentParent;
    std::map< int, int> persistentPair;
    std::map< int, int> criticalPaired;
    std::map< int, point<double> > extremaPositions;
    std::map< int, std::set<int> > saddleSets;


    std::map< int, double> persistence;
    std::map< pair<int,int>, int> ssps;
    std::map< int, int> ext_ids;
    std::map< int, int> sad_ids;
    std::map< int, int> in_moment;

    double *scalars;

    int numPoints;
    double max_region_size;

private:

    dataset()
    {    }


public:

    void operator=(dataset const&); // Don't implement
    dataset(dataset const&);              // Don't Implement
    void getPathKey(pair<int,int> key);

    double createArcPersistenceMap(bool area = false);
    void updateMoments(int reg_id, int px_id);
    void restoreState();

    void computeAPSD(double threshold);

    void simplifyExtrema(double area_threshold);
    void computePersistence(int numExtremaRemaining);

    void growNext(int id, int reg_id);
    void growAllRegions(double bf=0);
    void finalizeSSP();

    ostringstream textStream ;


};


#endif // DATASET_H
