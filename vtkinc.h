#ifndef VTKINC_H
#define VTKINC_H
#include <vtkAutoInit.h>



#define vtkRenderingCore_AUTOINIT 3(vtkInteractionStyle,vtkRenderingFreeType,vtkRenderingOpenGL)
#define vtkGUISupportQt_AUTOINIT 1(vtkGUISupportQt)
#define vtkFiltersCore_AUTOINIT 1(vtkFiltersCore)
#define vtkGUISupportQtOpenGL_AUTOINIT 1(vtkGUISupportQtOpenGL)
#define vtkFiltersExtraction_AUTOINIT 1(vtkFiltersExtraction)
#define vtkIOCore_AUTOINIT 1(vtkIOCore)
#define vtkCommonDataModel_AUTOINIT 1(vtkCommonDataModel)
#define vtkCommonExecutionModel_AUTOINIT 1(vtkCommonExecutionModel)
#define vtkImagingCore_AUTOINIT 1(vtkImagingCore)
#define vtkViewsCore_AUTOINIT 1(vtkViewsCore)
#define vtkFiltersHybrid_AUTOINIT 1(vtkFiltersHybrid)
#define vtkInteractionWidgets_AUTOINIT 1(vtkInteractionWidgets)
#define vtkFiltersTexture_AUTOINIT 1(vtkFiltersTexture)
#define vtkFiltersSources_AUTOINIT 1(vtkFiltersSources)
#define vtkCommonCore_AUTOINIT 1(vtkCommonCore)
#define vtkInfovisLayout_AUTOINIT 1(vtkInfovisLayout)
#define vtkCommonMisc_AUTOINIT 1(vtkCommonMisc)
#define vtkIOExport_AUTOINIT 1(vtkIOExport)
#define vtkInfovisCore_AUTOINIT 1(vtkInfovisCore)
#define vtkIOInfovis_AUTOINIT 1(vtkIOInfovis)
#define vtkViewsInfovis_AUTOINIT 1(vtkViewsInfovis)
//#define vtkIOImage_AUTOINIT 1(vtkIOImage)
#define vtkIOLegacy_AUTOINIT 1(vtkIOLegacy)

#include "vtkSetGet.h"
#include "vtkTextActor.h"
#include "vtkTextProperty.h"
#include "vtkTextSource.h"
#include "vtkVectorText.h"
#include "vtkWindowToImageFilter.h"
#include "vtkPNGWriter.h"
#include "vtkSmoothPolyDataFilter.h"
#include "vtkWindowedSincPolyDataFilter.h"
#include "vtkTubeFilter.h"
#include "vtkPointPicker.h"
#include "vtkParametricSpline.h"
#include "vtkParametricFunctionSource.h"
#include "vtkSplineFilter.h"
#include "vtkAssembly.h"
#include "vtkGradientFilter.h"
#include "vtkAssignAttribute.h"
#include "vtkArrowSource.h"
#include "vtkInformation.h"
#include "vtkScalarsToColors.h"
#include "vtkStreamLine.h"
#include "vtkStreamTracer.h"
#include "vtkExtractGrid.h"
#include "vtkRungeKutta4.h"
#include "vtkRungeKutta45.h"
#include "vtkPointSet.h"
#include "vtkPlaneSource.h"
#include "vtkPointSource.h"
#include "vtkDataSetWriter.h"
#include "vtkTable.h"
#include "vtkFloatArray.h"
#include "vtkContextView.h"
#include "vtkContextScene.h"
#include "vtkChart.h"
#include "vtkChartXY.h"
#include "vtkPlot.h"
#include "vtkPlotLine.h"
#include "vtkPolygon.h"
#include "vtkRegularPolygonSource.h"
#include "vtkEdgeListIterator.h"
#include "vtkRemoveIsolatedVertices.h"
#include "vtkProcrustesAlignmentFilter.h"
#include "vtkMultiBlockDataGroupFilter.h"
#include "vtkLandmarkTransform.h"
#include "vtkMultiBlockDataSet.h"
#include "vtkCellArray.h"
#include "vtkVertex.h"
#include "vtkPointSet.h"
#include "vtkExtractEdges.h"
#include "vtkPointData.h"
#include "vtkDoubleArray.h"
#include "vtkDataArray.h"
#include "vtkIntArray.h"
#include "vtkGraphToPolyData.h"
#include "vtkViewTheme.h"
#include "vtkGlyphSource2D.h"
#include "vtkScalarBarActor.h"
#include "vtkGlyph3D.h"
#include "vtkLine.h"
#include "vtkTriangle.h"
#include "vtkQuad.h"
#include "vtkErrorCode.h"
#include "vtkCallbackCommand.h"
#include "vtkMutableDirectedGraph.h"
#include "vtkPolyDataMapper.h"
#include "vtkGraphLayoutView.h"
#include "vtkActor.h"
#include "vtkDataObject.h"
#include "vtkDataSetReader.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkProperty.h"
#include "vtkPolyDataReader.h"
#include "vtkCubeSource.h"
#include "vtkInteractorStyleTrackball.h"
#include "vtkViewport.h"
#include "vtkTextActor3D.h"
#include "vtkPolyLine.h"
#include "vtkStructuredGridReader.h"
//#include "vtkStructuredGridToPolyDataFilter.h"
#include "vtkTriangleFilter.h"
#include "vtkGeometryFilter.h"
#include "vtkLineSource.h"
#include "vtkPolyDataWriter.h"
#include "vtkWarpScalar.h"
#include "vtkContourFilter.h"
#include "vtkPolyDataAlgorithm.h"
#include "vtkGraphLayoutView.h"
#include "vtkSpline.h"

#include "vtkAppendPolyData.h"
#include "vtkSphereSource.h"
#include "vtkStripper.h"
#include "vtkKochanekSpline.h"
#include "vtkSplineFilter.h"
#endif // VTKINC_H
