#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <sstream>
#include <assert.h>
#include <limits>
#include <cfloat>
#include <ctime>
#include <iterator>

int screen = 0;
time_t tstart,tend;
#define BEGIN_TIME tstart = time(0);
#define END_TIME tend = time(0);
#define DISP_TIME(x) cout<<x<<difftime(tend,tstart)<<endl;

void MainWindow::tagExtrema(vtkPolyData *poly)
{
    vtkDataArray *dataArray;
    dataArray = poly->GetPointData()->GetArray(0);
    if(scalarArrayNum != -1)
    {
        dataArray = poly->GetPointData()->GetArray(scalarArrayNum);
    }
    else if(scalarArrayName.size() != 0)
    {
        dataArray = poly->GetPointData()->GetArray(scalarArrayName.data());
    }
    if(!dataArray)
    {
        cout <<" Incorrect Scalar field Array name/number." << endl;
        return;
    }
    cout << "Read Scalar Array of Size: "<< dataArray->GetNumberOfTuples() << endl;


    int numPoints = poly->GetNumberOfPoints();
    assert(numPoints == dataArray->GetNumberOfTuples());
    data->scalars = new double[numPoints];
    double minval = DBL_MAX;
    double maxval = DBL_MIN;

    BEGIN_TIME;
    for(int i = 0; i < numPoints; i++ )
    {
        double value = invert ? dataArray->GetTuple1(i) : -1*dataArray->GetTuple1(i);
        if( !isnan(value))
        {
            minval = min(minval,value);
            maxval = max(maxval,value);
        }

    }
    END_TIME;
    DISP_TIME("Read Points: ");


    double newmaxval = DBL_MIN;
    double newminval = DBL_MAX;
    std::vector<order> ordervector;
    std::map<int,bool> isext;
    for(int i = 0; i < numPoints; i++)
    {
        order o;
        o.id = i;

        double value = invert ? dataArray->GetTuple1(i) : -1*dataArray->GetTuple1(i);
        value = ((value - minval)/(maxval-minval)) + 1;
        value = isnan(value) ? 1 : value;
        newmaxval = max(newmaxval,value);
        newminval = min(newminval,value);
        o.value = value;
        data->scalars[i] = value;
        ordervector.push_back(o);
        double p[3];
        poly->GetPoint(i,p);
        point<double> pt;
        pt.set(p[0],p[1],p[2]);
        data->extremaPositions[i]= pt;
        isext[i] = true;
        data->highestNbr[i]  = i;
    }


    // Go through all line cells in poly and fill nbrs
    vtkCellArray *lines = poly->GetLines();
    lines->InitTraversal();
    vtkSmartPointer<vtkIdList> list = vtkSmartPointer<vtkIdList>::New();

    BEGIN_TIME;
    while(lines->GetNextCell(list))
    {

        int i = list->GetId(0);
        int j = list->GetId(1);

        double val_i = data->scalars[i];
        double val_j = data->scalars[j];


        bool i_greater_j = (val_i > val_j || ((val_i==val_j) && (i > j) )) ? true : false;
        int h_nbr = i_greater_j ? j : i ;
        double h_nbr_scalar = data->scalars[data->highestNbr[h_nbr]];

        if(i_greater_j)
        {
            data->lowerRingSets[i].insert(j);
            if( h_nbr_scalar <= val_i )
            {
                data->highestNbr[j] = i;

            }

        }
        else
        {
            data->lowerRingSets[j].insert(i);
            if( h_nbr_scalar <= val_j )
            {
                data->highestNbr[i] = j;
            }

        }

        if(!i_greater_j)
        {

            isext[i] = false;
        }
        else
        {

            isext[j] = false;
        }

    }
    END_TIME;
    DISP_TIME("Read Edges: ");

    for(int i = 0; i < numPoints; i++)
    {
        int x = data->highestNbr[i];
        assert(x != -1 || isext[i]);
        assert(x != i || isext[i]);
        if(isext[i])
        {
            data->extremaSets.insert(i);
            data->persistence[i] = 1;
            data->persistentPair[i] = -1;
            data->persistentParent[i] = -1;
            data->labelListSets[i].insert(i);
        }
    }
    isext.clear();
    cout<< "Number of Extrema: " << data->extremaSets.size() << endl;
    data->numPoints = numPoints;

}




void MainWindow::displayBoundary()
{

    allCriticalPoints = vtkSmartPointer<vtkPolyData>::New();
    vtkSmartPointer<vtkPolyData> allLines = vtkSmartPointer<vtkPolyData>::New();
    vtkSmartPointer<vtkCellArray> lines = vtkSmartPointer<vtkCellArray>::New();

    vtkPoints *criticalPoints = vtkPoints::New();
    vtkSmartPointer<vtkCellArray> criticalCellArray = vtkSmartPointer<vtkCellArray>::New();
    vtkDoubleArray *criticalColors = vtkDoubleArray::New();
    criticalColors->SetNumberOfComponents(1);
    criticalColors->SetNumberOfTuples(data->numPoints); //squeeze this later

    vtkDoubleArray *segmentationArray = vtkDoubleArray::New();
    segmentationArray->SetNumberOfComponents(1);

    int black =0;
    int white = 0;
    for(int i = 0; i < data->numPoints; i++)
    {
        double id = 0;
        std::set<int> ls = data->labelListSets[i];
        std::set<int> newls;

        for(std::set<int>::iterator it = ls.begin(); it != ls.end(); it++)
        {
            int id = *it;

            while(data->persistentParent[id] != -1 && data->persistence[id]<=simUpto)
            {
                id = data->persistentParent[id];
            }
            newls.insert(id);

        }
        ls = newls;

        for(std::set<int>::iterator it = ls.begin(); it != ls.end(); )
        {
            int id = *it;
            if(data->persistence[id] <= simUpto)
            {
                ls.erase(it++);
            }
            else
                ++it;
        }


        assert(ls.size()<=2);

        for(std::set<int>::iterator it = ls.begin(); it != ls.end(); it++)
        {
            id += *it;//data->scalars[*it];
        }
        if(ls.size() > 1)
        {
            id = 0;
        }

        segmentationArray->InsertNextTuple1(id);
    }

    cout<<"black : " << black << endl;
    cout<<"white : " << white << endl;
    double diagLength = mesh->GetLength()/1;
    vtkSmartPointer<vtkAppendPolyData> appendFilter = vtkSmartPointer<vtkAppendPolyData>::New();


    for(std::set<int>::iterator i = data->extremaSets.begin(); i != data->extremaSets.end(); i++)
    {

        if(data->persistence[*i] > simUpto)
        {
            double* pts;
            point<double > pt = data->extremaPositions[*i];
            pts = pt.get3();
            vtkIdType cid = criticalPoints->InsertNextPoint(pts);
            vtkVertex *v = vtkVertex::New();
            v->GetPointIds()->SetId(0,cid);
            criticalCellArray->InsertNextCell(v);
            v->Delete();
            criticalColors->InsertTuple1(cid,0.0 );

            //cout << (int)n->cell_type << endl;
            ostringstream convert;
            convert << data->scalars[*i];
            vtkSmartPointer<vtkTextActor3D> text = vtkSmartPointer<vtkTextActor3D>::New();
            text->SetInput(convert.str().data());
            text->SetScale(0.001*diagLength,0.001*diagLength,0.001*diagLength);
            vtkSmartPointer<vtkTextProperty> textprop = vtkSmartPointer<vtkTextProperty>::New();
            textprop->BoldOn();
            textprop->SetFontSize(14);
            textprop->SetFontFamilyToTimes();
            textprop->SetVerticalJustificationToTop();
            //textprop->SetColor(0.1,0,1);
            text->SetTextProperty(textprop);
            text->SetPosition(pts);
            dagrenderer2->AddActor(text);


            ostringstream convert2;
            convert2 << *i;//data->scalars[*i];
            vtkSmartPointer<vtkTextActor3D> text2 = vtkSmartPointer<vtkTextActor3D>::New();
            text2->SetInput(convert2.str().data());
            text2->SetScale(0.001*diagLength,0.001*diagLength,0.001*diagLength);
            text2->SetTextProperty(textprop);
            text2->SetPosition(pts);
            dagrenderer->AddActor(text2);

            // if(data->scalars[*i] >=1.5 )
            {

                vtkSmartPointer<vtkSphereSource> sph = vtkSmartPointer<vtkSphereSource>::New();
                sph->SetCenter(pt.get3());
                sph->SetRadius(diagLength*0.005);
                sph->Update();

                appendFilter->AddInputData(sph->GetOutput());
                appendFilter->Update();
            }



        }
    }

    vtkSmartPointer<vtkPoints> line_pts = vtkSmartPointer<vtkPoints>::New();

    for(SSP_MAP::iterator it = data->ssp_min.begin(); it != data->ssp_min.end() ; it++)
    {
        int m1 = (it->first).first;
        int m2 = (it->first).second;
        int s  = ((it->second).first).first;
        double edge_imp = ((it->second).first).second;

        if(data->extremaSets.find(m1) == data->extremaSets.end())
            continue;

        if(data->extremaSets.find(m2) == data->extremaSets.end())
            continue;

        if(data->persistence[m1]>=simUpto && data->persistence[m2]>=simUpto && edge_imp >= 1)
        {
            point<double> p = data->extremaPositions[s];
            point<double> p1 = data->extremaPositions[m1];
            point<double> p2 = data->extremaPositions[m2];
            vtkSmartPointer<vtkLineSource> ls1 = vtkSmartPointer<vtkLineSource>::New();
            ls1->SetPoint1(p1.get3());
            ls1->SetPoint2(p.get3());
            ls1->Update();

            vtkSmartPointer<vtkLineSource> ls2 = vtkSmartPointer<vtkLineSource>::New();
            ls2->SetPoint1(p.get3());
            ls2->SetPoint2(p2.get3());
            ls2->Update();


            vtkSmartPointer<vtkPolyDataMapper> lm1 = vtkSmartPointer<vtkPolyDataMapper>::New();
            lm1->SetInputData(ls1->GetOutput());
            lm1->Update();
            vtkSmartPointer<vtkPolyDataMapper> lm2 = vtkSmartPointer<vtkPolyDataMapper>::New();
            lm2->SetInputData(ls2->GetOutput());
            lm2->Update();
            vtkSmartPointer<vtkActor> la1 = vtkSmartPointer<vtkActor>::New();
            la1->SetMapper(lm1);
            vtkSmartPointer<vtkActor> la2 = vtkSmartPointer<vtkActor>::New();
            la2->SetMapper(lm2);

            la1->GetProperty()->SetOpacity(0.5);
            la2->GetProperty()->SetOpacity(0.5);

            dagrenderer2->AddActor(la1);
            dagrenderer2->AddActor(la2);
            dagrenderer->AddActor(la1);
            dagrenderer->AddActor(la2);

            //p.set(0.5*(p1.getx()+p2.getx()),0.5*(p1.gety()+p2.gety()),0.5*(p1.getz()+p2.getz()));

            vtkIdType id1 = line_pts->InsertNextPoint(p1.get3());
            vtkIdType id2 = line_pts->InsertNextPoint(p.get3());
            vtkIdType id3 = line_pts->InsertNextPoint(p2.get3());
            vtkSmartPointer<vtkLine> line1 =
                    vtkSmartPointer<vtkLine>::New();
            line1->GetPointIds()->SetId(0,id1);
            line1->GetPointIds()->SetId(1,id2);
            vtkSmartPointer<vtkLine> line2 =
                    vtkSmartPointer<vtkLine>::New();
            line2->GetPointIds()->SetId(0,id2);
            line2->GetPointIds()->SetId(1,id3);
            lines->InsertNextCell(line1);
            lines->InsertNextCell(line2);

        }
    }

    allLines->SetPoints(line_pts);
    allLines->SetLines(lines);
    allLines->Modified();

    allCriticalPoints->SetPoints(criticalPoints);
    allCriticalPoints->SetVerts(criticalCellArray);
    allCriticalPoints->Modified();
    criticalColors->Squeeze();
    allCriticalPoints->GetPointData()->SetScalars(criticalColors);

    criticalPoints->Delete();
    criticalColors->Delete();


    vtkSmartPointer<vtkPolyDataMapper> cmapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    cmapper->SetInputData(allCriticalPoints);
    cmapper->SetScalarRange(0,1);

    cmapper->ScalarVisibilityOn();
    cmapper->Update();
    vtkSmartPointer<vtkActor> cactor = vtkSmartPointer<vtkActor>::New();
    cactor->SetMapper(cmapper);

    cactor->GetProperty()->SetPointSize(6.0);


    int numpts = mesh->GetPointData()->GetNumberOfTuples();
    int numsg = segmentationArray->GetNumberOfTuples();
    cout<< "A : "<<numpts << " B: " <<numsg << endl;
    assert(mesh->GetPointData()->GetNumberOfTuples() == segmentationArray->GetNumberOfTuples());
    segmentationArray->SetName("binary");
    mesh->GetPointData()->AddArray(segmentationArray);
    //mesh->GetPointData()->SetScalars(segmentationArray);
    mesh->GetPointData()->Modified();
    mesh->Modified();
    double rng[2];
    mesh->GetScalarRange(rng);

    vtkSmartPointer<vtkContourFilter> contour = vtkSmartPointer<vtkContourFilter>::New();

    contour->SetInputData(mesh);
    contour->SetValue(0, (rng[0]+rng[1])/2.0);
    contour->Update();
    meshMapper = vtkSmartPointer<vtkPolyDataMapper>::New();

    meshMapper->SetInputData(contour->GetOutput());
    double range[2];
    mesh->GetScalarRange(range);
    meshMapper->SetScalarRange(range);
    meshMapper->Update();
    meshActor = vtkSmartPointer<vtkActor>::New();
    meshActor->SetMapper(meshMapper);
    meshActor->GetProperty()->SetRepresentationToWireframe();
    meshActor->GetProperty()->SetOpacity(0.5);

    dagrenderer->AddActor(meshActor);

    dagrenderer->ResetCamera();



    dagrenderer->AddActor(cactor);
    dagrenderer2->AddActor(cactor);

    this->ui->qvtkWidget_3->update();
    this->ui->qvtkWidget_4->update();

    vtkSmartPointer<vtkPolyDataWriter> wr = vtkSmartPointer<vtkPolyDataWriter>::New();
    wr->SetInputData(allLines);

    std::string fn = filename;
    fn.replace(fn.end()-4,fn.end(),"_lg.vtk");
    wr->SetFileName(fn.data());
    wr->SetFileTypeToBinary();
    wr->Update();
    wr->Write();

    vtkSmartPointer<vtkPolyDataWriter> wr2 = vtkSmartPointer<vtkPolyDataWriter>::New();
    wr2->SetInputData(appendFilter->GetOutput());

    std::string fn2 = filename;
    fn2.replace(fn2.end()-4,fn2.end(),"_cp.vtk");
    wr2->SetFileName(fn2.data());
    wr2->SetFileTypeToBinary();
    wr2->Update();
    wr2->Write();


    vtkSmartPointer<vtkStripper> stripper =
            vtkSmartPointer<vtkStripper>::New();
    stripper->SetInputData(allLines);
    stripper->Update();

    vtkSmartPointer<vtkKochanekSpline> spline =
            vtkSmartPointer<vtkKochanekSpline>::New();
    spline->SetDefaultTension(.5);


    vtkSmartPointer<vtkSplineFilter> sf =
            vtkSmartPointer<vtkSplineFilter>::New();
    sf->SetInputData(stripper->GetOutput());
    sf->SetSubdivideToSpecified();
    sf->SetNumberOfSubdivisions(50);
    sf->SetSpline(spline);
    //sf->GetSpline()->ClosedOn();
    sf->Update();

    vtkSmartPointer<vtkTubeFilter> tubes =
            vtkSmartPointer<vtkTubeFilter>::New();
    tubes->SetInputConnection( sf->GetOutputPort());
    tubes->SetNumberOfSides(8);
    tubes->SetRadius(0.002*diagLength);
    tubes->Update();

    vtkSmartPointer<vtkPolyDataWriter> wr3 = vtkSmartPointer<vtkPolyDataWriter>::New();
    wr3->SetInputData(tubes->GetOutput());

    std::string fn3 = filename;
    fn3.replace(fn3.end()-4,fn3.end(),"_gr.vtk");
    wr3->SetFileName(fn3.data());
    wr3->SetFileTypeToBinary();
    wr3->Update();
    wr3->Write();

    cout <<"Finished Displaying boundary "<< endl;



}




MainWindow::MainWindow(QWidget *parent) :QMainWindow(parent), ui(new Ui::MainWindow)
{
    //Initialize
    srand(time(0));
    ui->setupUi(this);

    data = &(dataset::getInstance());
    //Setup renderers
    dagrenderer2 = vtkSmartPointer<vtkRenderer>::New();
    dagrenderer = vtkSmartPointer<vtkRenderer>::New();

    renWin = this->ui->qvtkWidget_3->GetRenderWindow();
    renInt = this->ui->qvtkWidget_3->GetInteractor();
    dagrenderer2->SetBackground(0.5,0.5,0.5);
    dagrenderer->SetBackground(0.5,0.5,0.5);
    this->ui->qvtkWidget_3->GetRenderWindow()->AddRenderer(dagrenderer);
    this->ui->qvtkWidget_4->GetRenderWindow()->AddRenderer(dagrenderer2);

    //Command Line Input
    int argsize  = QApplication::arguments().size();
    if(argsize < 2)
    {
        cout<<"Usage: ceg filename[foo.vtk] persistence_threshold[double(default=1)] maximum/minimum graph[1/0(default)] array number/array name[int/string(default 0)]"<<endl;
        exit(0);
    }
    filename.assign(QApplication::arguments().at(1).toStdString());
    renWin->SetWindowName(filename.data());
    double pers = 1;
    scalarArrayNum = 0;
    if(argsize > 2){
        pers = QApplication::arguments().at(2).toDouble();
        if(argsize > 3)
        {
            invert = QApplication::arguments().at(3).toInt();
        }
        if(argsize > 4)
        {
            scalarArrayName = QApplication::arguments().at(4).toStdString();
            bool ok;
            scalarArrayNum = QApplication::arguments().at(4).toInt(&ok);
            if(!ok)
                scalarArrayNum = -1;
        }


    }
    data->invert = invert;
    data->simUpto = pers;
    simUpto = pers;


    //Data Reader


    vtkSmartPointer<vtkDataSetReader> drdr = vtkSmartPointer<vtkDataSetReader>::New();
    drdr->SetFileName(filename.data());
    drdr->ReadAllColorScalarsOn();
    drdr->Update();
    omesh = drdr->GetOutput();
    vtkSmartPointer<vtkIntArray> idArray = vtkSmartPointer<vtkIntArray>::New();
    idArray->SetName("ids");
    int np = omesh->GetNumberOfPoints();
    for(int i = 0; i < np; i++)
    {
        idArray->InsertNextTuple1(i);
    }
    omesh->GetPointData()->AddArray(idArray);
    omesh->GetPointData()->Modified();
    omesh->Modified();
    vtkSmartPointer<vtkExtractEdges> edgeFilter = vtkSmartPointer<vtkExtractEdges>::New();
    /*
//    vtkSmartPointer<vtkGeometryFilter> polyfilter = vtkSmartPointer<vtkGeometryFilter>::New();
//    vtkSmartPointer<vtkTriangleFilter> tfilter =vtkSmartPointer<vtkTriangleFilter>::New();
//    polyfilter->SetInputData(drdr->GetOutput());
//    polyfilter->Update();

//    tfilter->SetInputData(polyfilter->GetOutput());
//    tfilter->Update();
*/

    edgeFilter->SetInputData(omesh);
    edgeFilter->Update();
    mesh = edgeFilter->GetOutput();
    assert(mesh->GetNumberOfPoints() == np);

    mesh->BuildCells();
    mesh->BuildLinks();
    dagrenderer->ResetCamera();

    BEGIN_TIME;

    tagExtrema(mesh);
    END_TIME;
    DISP_TIME("Tag Extrema: ");



    this->ui->doubleSpinBox->setValue(pers);

    // simplify, update info for dump
    BEGIN_TIME;
    on_simplifyUpto_clicked();
    END_TIME;
    DISP_TIME("Complete Simplification: ");

    displayBoundary();
    this->ui->qvtkWidget_3->update();
    this->ui->qvtkWidget_4->update();


    dump_info_on_close();

    // Supress UI

    // QMetaObject::invokeMethod(this, "close", Qt::QueuedConnection);
    cout <<"DONE."<<endl;

}


MainWindow::~MainWindow()
{

    delete ui;
}


void momentsToStream(std::vector<moments> vec, ofstream& o)
{
    for(unsigned int i = 0; i < vec.size(); i++)
    {
        moments m = vec[i];
        o<<m.m000<<endl;
        o<<m.m002<<endl;
        o<<m.m011<<endl;
        o<<m.m020<<endl;
        o<<m.m101<<endl;
        o<<m.m110<<endl;
        o<<m.m200<<endl;
        o<<m.m001<<endl;
        o<<m.m010<<endl;
        o<<m.m100<<endl;
    }
}
template<class T>
void vecToStream(std::vector<T> vec, ofstream& o)
{
    for(int i = 0; i < vec.size(); i++)
    {
        o<<vec[i]<<endl;
    }
}
void MainWindow::mapToStream2(map<int, set<int> > map, ofstream& o)
{
    std::map<int, std::set<int> >::iterator it = map.begin();
    while(it != map.end())
    {

        int m = (it->first);
        set<int> set = it->second;
        m = data->ext_ids[m];//std::distance(data->extremaSets.begin(),data->extremaSets.find(m));
        o<<m<<endl;
        for(std::set<int>::iterator sit = set.begin(); sit != set.end(); sit++)
        {
            int x = data->ext_ids[*sit];//std::distance(data->extremaSets.begin(),data->extremaSets.find(*sit));
            o<<x<<endl;
        }
        o<<-1<<endl;
        it++;

    }

}
void mapToStream(SSP_MAP map, ofstream& o)
{
    SSP_MAP::iterator it = map.begin();
    while(it != map.end())
    {

        int m1 = (it->first).first;
        int m2 = (it->first).second;
        int s  = ((it->second).first).first;
        double p = ((it->second).first).second;
        double l = ((it->second).second).first;
        double h = ((it->second).second).second;
        o<<m1<<endl;
        o<<m2<<endl;
        o<<s<<endl;
        o<<p<<endl;
        o<<l<<endl;
        o<<h<<endl;
        it++;

    }

}

void MainWindow::dumpInformation()
{

    assert(data);
    std::vector<double> min_scalars_vector;
    std::vector<double> sad_scalars_vector;
    std::vector<double> min_prop_vector;
    std::vector<double> sad_prop_vector;
    std::vector<double> min_pos_vector;
    std::vector<double> sad_pos_vector;
    std::vector<int> min_pair_vector;
    std::vector<int> sad_pair_vector;
    std::vector<double> radius;
    std::vector<std::vector<int> > min_labels;
    std::vector<int>    parent;
    std::vector<moments> min_moments;


    for(std::set<int>::iterator it = data->extremaSets.begin(); it != data->extremaSets.end(); it++)

    {


        min_scalars_vector.push_back(data->scalars[*it]);
        min_prop_vector.push_back(data->persistence[*it]);
        min_moments.push_back(data->RegionMoments[*it]);
        point<double> p = data->extremaPositions[*it];

        min_pos_vector.push_back(p.getx());
        min_pos_vector.push_back(p.gety());
        min_pos_vector.push_back(p.getz());
        int pp = data->persistentPair[*it];
        assert(pp<=0||((data->vsaddles.find(pp) != data->vsaddles.end())));
        int x =  data->sad_ids[pp];
        min_pair_vector.push_back(pp!= -1?x:-1);
        int par = data->persistentParent[*it];
        int y = data->ext_ids[par];
        if(par == -1)
            parent.push_back(-1);
        else
            parent.push_back(y);

        radius.push_back(data->regionSize[*it]);
        std::vector<int> lab;

        lab.push_back(-1);
        min_labels.push_back(lab);

    }

    for(std::set<int>::iterator it = data->vsaddles.begin(); it != data->vsaddles.end(); it++)
    {

        sad_scalars_vector.push_back(data->scalars[*it]);
        sad_prop_vector.push_back(0.5);

        point<double> p = data->extremaPositions[*it];
        sad_pos_vector.push_back(p.getx());
        sad_pos_vector.push_back(p.gety());
        sad_pos_vector.push_back(p.getz());
        sad_pair_vector.push_back(-99);

    }

    std::string dumpFileName;
    dumpFileName.assign(filename.data());
    dumpFileName.replace(dumpFileName.end()-3,dumpFileName.end(),"min_scalars");
    ofstream dumpFile(dumpFileName.data(),ios::trunc|ios::out);
    vecToStream<double>(min_scalars_vector,dumpFile);
    dumpFile.close();

    dumpFileName.assign(filename.data());
    dumpFileName.replace(dumpFileName.end()-3,dumpFileName.end(),"parent");
    dumpFile.open(dumpFileName.data(),ios::trunc|ios::out);
    vecToStream<int>(parent,dumpFile);
    dumpFile.close();

    dumpFileName.assign(filename.data());
    dumpFileName.replace(dumpFileName.end()-3,dumpFileName.end(),"radius");
    dumpFile.open(dumpFileName.data(),ios::trunc|ios::out);
    vecToStream<double>(radius,dumpFile);
    dumpFile.close();


    dumpFileName.assign(filename.data());
    dumpFileName.replace(dumpFileName.end()-3,dumpFileName.end(),"sad_scalars");
    dumpFile.open(dumpFileName.data(),ios::trunc|ios::out);
    vecToStream<double>(sad_scalars_vector,dumpFile);
    dumpFile.close();

    dumpFileName.assign(filename.data());
    dumpFileName.replace(dumpFileName.end()-3,dumpFileName.end(),"min_pers");
    dumpFile.open(dumpFileName.data(),ios::trunc|ios::out);
    vecToStream<double>(min_prop_vector,dumpFile);
    dumpFile.close();


    dumpFileName.assign(filename.data());
    dumpFileName.replace(dumpFileName.end()-3,dumpFileName.end(),"sad_pers");
    dumpFile.open(dumpFileName.data(),ios::trunc|ios::out);
    vecToStream<double>(sad_prop_vector,dumpFile);
    dumpFile.close();

    dumpFileName.assign(filename.data());
    dumpFileName.replace(dumpFileName.end()-3,dumpFileName.end(),"min_pos");
    dumpFile.open(dumpFileName.data(),ios::trunc|ios::out);
    vecToStream<double>(min_pos_vector,dumpFile);;
    dumpFile.close();

    dumpFileName.assign(filename.data());
    dumpFileName.replace(dumpFileName.end()-3,dumpFileName.end(),"sad_pos");
    dumpFile.open(dumpFileName.data(),ios::trunc|ios::out);
    vecToStream<double>(sad_pos_vector,dumpFile);;
    dumpFile.close();

    dumpFileName.assign(filename.data());
    dumpFileName.replace(dumpFileName.end()-3,dumpFileName.end(),"ssp");
    dumpFile.open(dumpFileName.data(),ios::trunc|ios::out);
    mapToStream( data->ssp_min,dumpFile);;
    dumpFile.close();

    dumpFileName.assign(filename.data());
    dumpFileName.replace(dumpFileName.end()-3,dumpFileName.end(),"min_pair");
    dumpFile.open(dumpFileName.data(),ios::trunc|ios::out);
    vecToStream<int>(min_pair_vector,dumpFile);;
    dumpFile.close();


    dumpFileName.assign(filename.data());
    dumpFileName.replace(dumpFileName.end()-3,dumpFileName.end(),"sad_pair");
    dumpFile.open(dumpFileName.data(),ios::trunc|ios::out);
    vecToStream<int>(sad_pair_vector,dumpFile);;
    dumpFile.close();

    dumpFileName.assign(filename.data());
    dumpFileName.replace(dumpFileName.end()-3,dumpFileName.end(),"min_labels");
    dumpFile.open(dumpFileName.data(),ios::trunc|ios::out);
    for(unsigned int i = 0; i < min_labels.size(); i++)
        vecToStream<int>(min_labels[i],dumpFile);
    dumpFile.close();

    dumpFileName.assign(filename.data());
    dumpFileName.replace(dumpFileName.end()-3,dumpFileName.end(),"cp");
    dumpFile.open(dumpFileName.data(),ios::trunc|ios::out);
    mapToStream2(data->candidateSets,dumpFile);
    dumpFile.close();


    dumpFileName.assign(filename.data());
    dumpFileName.replace(dumpFileName.end()-3,dumpFileName.end(),"cg");
    dumpFile.open(dumpFileName.data(),ios::trunc|ios::out);
    mapToStream2(data->neighbours,dumpFile);
    dumpFile.close();


    dumpFileName.assign(filename.data());
    dumpFileName.replace(dumpFileName.end()-3,dumpFileName.end(),"cc");
    dumpFile.open(dumpFileName.data(),ios::trunc|ios::out);
    mapToStream2(data->children,dumpFile);
    dumpFile.close();

    dumpFileName.assign(filename.data());
    dumpFileName.replace(dumpFileName.end()-3,dumpFileName.end(),"moments");
    dumpFile.open(dumpFileName.data(),ios::trunc|ios::out);
    momentsToStream(min_moments,dumpFile);
    dumpFile.close();


}



void MainWindow::on_generateScreenshot_clicked()
{
    // Screenshot

    this->ui->qvtkWidget_3->GetRenderWindow()->Render();
    this->ui->qvtkWidget_4->GetRenderWindow()->Render();

    vtkSmartPointer<vtkWindowToImageFilter> windowToImageFilter =
            vtkSmartPointer<vtkWindowToImageFilter>::New();
    windowToImageFilter->SetInput(this->ui->qvtkWidget_3->GetRenderWindow());
    windowToImageFilter->SetMagnification(1); //set the resolution of the output image (3 times the current resolution of vtk render window)
    windowToImageFilter->SetInputBufferTypeToRGBA(); //also record the alpha (transparency) channel
    windowToImageFilter->Update();

    vtkSmartPointer<vtkPNGWriter> writer =
            vtkSmartPointer<vtkPNGWriter>::New();
    std::ostringstream s;
    s<<"/home/vidya/Pictures/debug/"<<screen<<".png";
    writer->SetFileName(s.str().data());
    writer->SetInputConnection(windowToImageFilter->GetOutputPort());
    writer->Write();

    vtkSmartPointer<vtkWindowToImageFilter> windowToImageFilter2 =
            vtkSmartPointer<vtkWindowToImageFilter>::New();
    windowToImageFilter2->SetInput(this->ui->qvtkWidget_4->GetRenderWindow());
    windowToImageFilter2->SetMagnification(1);
    windowToImageFilter2->SetInputBufferTypeToRGBA();
    windowToImageFilter2->Update();

    vtkSmartPointer<vtkPNGWriter> writer2 =
            vtkSmartPointer<vtkPNGWriter>::New();
    std::ostringstream s2;
    s2<<"/home/vidya/Pictures/debug/"<<screen<<"_dag.png";
    writer2->SetFileName(s2.str().data());
    writer2->SetInputConnection(windowToImageFilter2->GetOutputPort());
    writer2->Write();

    dagrenderer->ResetCamera();
    dagrenderer2->ResetCamera();
    this->ui->qvtkWidget_3->update();
    this->ui->qvtkWidget_4->update();

    screen++;
}


int global_cnt = 0;


void MainWindow::dumpPersistenceStatistics()
{

    //file
    std::string dumpFileName;
    dumpFileName.assign(filename.data());
    dumpFileName.replace(dumpFileName.end()-4,dumpFileName.end(),"_stat_low.csv");
    ofstream fout(dumpFileName.data(),ios::trunc|ios::out);

    for(std::set<int>::iterator it = data->extremaSets.begin(); it !=  data->extremaSets.end(); it++)
    {
        fout<<data->persistence[*it]<<" , ";
    }
    fout<<endl;
    fout.close();

    dumpFileName.assign(filename.data());
    dumpFileName.replace(dumpFileName.end()-4,dumpFileName.end(),"_stat_high.csv");
    fout.open(dumpFileName.data(),ios::trunc|ios::out);
    for(SSP_MAP::iterator it = data->ssp_min.begin(); it != data->ssp_min.end(); it++)
    {
        int m1 = (it->first).first;
        int m2 = (it->first).second;
        double imp = ((it->second).first).second;
        double dist = ((it->second).second).first;
        double p1 = data->persistence[m1];
        double p2 = data->persistence[m2];

        fout<< imp << " , "<<dist<<" , "<<p1<<" , " <<p2<<endl;
    }
    fout<<endl;
    fout.close();


    dumpFileName.assign(filename.data());
    dumpFileName.replace(dumpFileName.end()-4,dumpFileName.end(),"_stat_vol.csv");
    fout.open(dumpFileName.data(),ios::trunc|ios::out);

    for(std::set<int>::iterator it = data->extremaSets.begin(); it !=  data->extremaSets.end(); it++)
    {

        moments momA = data->RegionMoments[*it];

        std::set<int> aChildren = data->children[*it];
        aChildren.erase(-1);
        bool till = false;
        do
        {
            till = false;
            std::set<int> anew = aChildren;
            for(std::set<int>::iterator it = aChildren.begin(); it != aChildren.end(); it++)
            {
                std::set<int> aset = data->children[*it];
                aset.erase(-1);
                for(std::set<int>::iterator it = aset.begin(); it != aset.end(); it++)
                {
                    if(anew.find(*it) == anew.end())
                    {
                        anew.insert(*it);
                        till = true;
                    }

                }
            }
            aChildren = anew;
        }
        while(till);


        for(std::set<int>::iterator it = aChildren.begin(); it != aChildren.end(); it++)
        {
            {
                momA = momA + data->RegionMoments[*it];
            }
        }
        fout << momA.m000 <<  endl;

    }
    fout<<endl;
    fout.close();



}

void MainWindow::on_simplifyUpto_clicked()
{

    //Simplify the really low persistent extrema

    BEGIN_TIME;
    data->growAllRegions(0);
    END_TIME;
    DISP_TIME("Region Growing: ");
    BEGIN_TIME;
    data->simplifyExtrema(1e-5);
    data->computePersistence(INT_MAX);
    simUpto = data->simUpto;
    data->restoreState();

    cout<<"apsd"<<endl;
    data->ssp_min = data->ssptemp;
    data->computeAPSD(1);
    cout<<"Number of Extrema : "<<data->extremaSets.size()<<endl;

    //IDS
    int id = 0;
    for(std::set<int>::iterator it =  data->extremaSets.begin(); it != data->extremaSets.end(); it++)
    {
        data->ext_ids[*it]=id++;
    }
    id = 0;
    for(std::set<int>::iterator it =  data->vsaddles.begin(); it != data->vsaddles.end(); it++)
    {
        data->sad_ids[*it]=id++;
    }

    END_TIME;
    DISP_TIME("Persistence Comp: ");



}


void MainWindow::dumpSegmentedData()
{
    std::map<int,int> extremaToPID;
    std::map<int,int> PIDtoExtrema;
    std::vector<order> vecorder;
    for(std::set<int>::iterator it = data->extremaSets.begin(); it != data->extremaSets.end(); it++)
    {
        order o;
        o.id = *it;
        o.value = data->persistence[o.id];
        vecorder.push_back(o);
    }
    // DONT sort, extrema are pushed out it whatever order
    // id they have in the set, not persistence ordeirng
    // else labelling will go haywire.

    //std::sort(vecorder.begin(), vecorder.end(), order_greater());
    for(unsigned int i = 0; i < vecorder.size(); i++)
    {
        order o = vecorder[i];
        extremaToPID[o.id] = i+1;
        PIDtoExtrema[i+1] = o.id;
    }
    map<int,int> segmap;

    vtkFloatArray *criticalArray = vtkFloatArray::New();
    criticalArray->SetNumberOfComponents(1);
    criticalArray->SetNumberOfTuples(data->numPoints);


    vtkDoubleArray *gradientArray = vtkDoubleArray::New();
    gradientArray->SetNumberOfComponents(3);
    gradientArray->SetNumberOfTuples(data->numPoints);


    vtkFloatArray *segmentationArray = vtkFloatArray::New();
    segmentationArray->SetNumberOfComponents(1);
    segmentationArray->SetNumberOfValues(data->numPoints);


    vtkIntArray *momentSegmentationArray = vtkIntArray::New();
    momentSegmentationArray->SetNumberOfComponents(1);
    momentSegmentationArray->SetNumberOfValues(data->numPoints);

    vtkDoubleArray *dataArray = vtkDoubleArray::New();
    dataArray->SetNumberOfComponents(1);
    dataArray->SetNumberOfValues(data->numPoints);

    vtkDoubleArray *strengthArray = vtkDoubleArray::New();
    strengthArray->SetNumberOfComponents(1);
    strengthArray->SetNumberOfValues(data->numPoints);



    vtkSmartPointer<vtkIntArray> idArray = (vtkIntArray*)mesh->GetPointData()->GetArray("ids");
    assert(idArray->GetSize()==data->numPoints);

    std::vector<order> allorder;
    for(int i =0; i < data->numPoints; i++)
    {
        order o;
        o.id = i;
        o.value = data->scalars[i];
        allorder.push_back(o);
    }
    std::sort(allorder.begin(),allorder.end(),order_greater());
    for(unsigned int i = 0; i < allorder.size(); i++)
    {
        int id = allorder[i].id;

        if(data->labelListSets[id].size()==0)
        {

            int nb_id = data->highestNbr[id];
            assert(nb_id >=0);
            std::set<int> lsset = data->labelListSets[nb_id];
            while(data->labelListSets[nb_id].size()==0 && nb_id != id)
            {
                nb_id = data->highestNbr[nb_id];
            }
            data->labelListSets[id] = data->labelListSets[nb_id];


        }
    }


    for(int i = 0; i < data->numPoints; i++)
    {
        vtkIdType o_id = (vtkIdType)(idArray->GetTuple1(i));
        assert(o_id >=0 && o_id < data->numPoints);
        // set the data array to have normalized values

        dataArray->SetTuple1(o_id,data->scalars[i]);
        momentSegmentationArray->SetTuple1(o_id,0);
        criticalArray->SetTuple1(o_id,0);
        if(data->extremaSets.find(i) != data->extremaSets.end())
            criticalArray->SetTuple1(o_id,1);
        if(data->vsaddles.find(i) != data->vsaddles.end())
            criticalArray->SetTuple1(o_id,0.5);


        int i_nbr = data->highestNbr[i];
        point<double> p2 = data->extremaPositions[i];
        point<double> p1 = data->extremaPositions[i_nbr];
        double g0 = p1.getx()-p2.getx();
        double g1 = p1.gety()-p2.gety();
        double g2 = p1.getz()-p2.getz();
        gradientArray->SetTuple3(o_id,g0,g1,g2);


        std::set<int> ls = data->labelListSets[i];

        assert(ls.size() > 0);
        std::set<int> newls = ls;
        for(std::set<int>::iterator it = ls.begin(); it != ls.end(); it++)
        {
            int id = *it;

            while(data->persistentParent[id] != -1 && data->persistence[id]<=data->simUpto)
            {
                id = data->persistentParent[id];
            }

            newls.insert(id);

        }
        ls = newls;
        int best_id = -1;
        double curr_max_pers = 0;
        for(std::set<int>::iterator it = ls.begin(); it != ls.end(); )
        {
            int id = *it;
            if(data->persistence[id] <= data->simUpto)
            {
                ls.erase(it++);
            }
            else
            {
                if(data->persistence[id] > curr_max_pers)
                {
                    curr_max_pers = data->persistence[id];
                    best_id = id;
                }
                ++it;
            }
        }
        assert(best_id != -1);
        assert(ls.size() > 0);


        int id = best_id;
        if(data->extremaSets.find(id)==data->extremaSets.end())
        {
            segmentationArray->SetTuple1(o_id,0);
            continue;
        }
        assert(extremaToPID.find(id) != extremaToPID.end() );
        int seg_id = extremaToPID[id];
        assert(seg_id != 0);
        segmap[id] = segmap[id]+1;
        segmentationArray->SetTuple1(o_id,seg_id);

        if(data->in_moment[i])
        {
            momentSegmentationArray->SetTuple1(o_id,seg_id);
        }
        else
        {
            momentSegmentationArray->SetTuple1(o_id,0);
        }



    }

    for(int i = 0; i < data->numPoints; i++)
    {
        vtkIdType o_id = (vtkIdType)(idArray->GetTuple1(i));
        assert(o_id >=0 && o_id < data->numPoints);
        int seg_id = (int)momentSegmentationArray->GetTuple1(o_id);
        if(seg_id < 1)
            continue;
        int ext_id = PIDtoExtrema[seg_id];
        assert(data->extremaSets.find(ext_id) != data->extremaSets.end());
        assert(extremaToPID[ext_id]==seg_id);
        std::set<int> ls = data->labelListSets[i];
        //  assert(ls.size()==1);
        int id = *ls.begin();
        // does id end up through any of its candidate Parents(other than what is in ext_set) at any other final seg id ?
        std::set<int> parents = data->candidateSets[id];

        parents.erase(ext_id);
        std::set<int> result;
        std::set_intersection(parents.begin(),parents.end(),data->extremaSets.begin(),data->extremaSets.end(),
                              std::inserter(result,result.begin()));
        for(std::set<int>::iterator it = parents.begin(); it != parents.end();)
        {
            if(data->extremaSets.find(*it) != data->extremaSets.end())
            {
                parents.erase(it++);
            }
            else
                ++it;
        }

        bool done = true;
        do{
            done = true;
            std::set<int> p = parents;
            for(std::set<int>::iterator it = parents.begin(); it != parents.end(); it++)
            {
                if(data->extremaSets.find(*it) != data->extremaSets.end())
                    continue;
                std::set<int> p_ = data->candidateSets[*it];
                p.insert(p_.begin(),p_.end());
            }
            if(p.size() > parents.size())
                done = false;
            parents = p;

        }while(!done);

        std::set_intersection(parents.begin(),parents.end(),data->extremaSets.begin(),data->extremaSets.end(),
                              std::inserter(result,result.end()));

        result.erase(ext_id);
        //double strength = (double)(data->extremaSets.size()-result.size())/(double)data->extremaSets.size();
        double strength = result.size()==0 ? 1 : 0;

        strengthArray->SetTuple1(o_id,strength);


    }



    segmentationArray->SetName("segments");
    omesh->GetPointData()->AddArray(segmentationArray);
    omesh->GetPointData()->Modified();
    omesh->Modified();



    momentSegmentationArray->SetName("m_segments");
    omesh->GetPointData()->AddArray(momentSegmentationArray);
    omesh->GetPointData()->Modified();
    omesh->Modified();



    dataArray->SetName("data");
    omesh->GetPointData()->AddArray(dataArray);
    omesh->GetPointData()->Modified();
    omesh->Modified();



    strengthArray->SetName("strength");
    omesh->GetPointData()->AddArray(strengthArray);
    omesh->GetPointData()->Modified();
    omesh->Modified();

    criticalArray->SetName("critical");
    omesh->GetPointData()->AddArray(criticalArray);
    omesh->GetPointData()->Modified();
    omesh->Modified();


    gradientArray->SetName("pgrad");
    omesh->GetPointData()->AddArray(gradientArray);
    omesh->GetPointData()->Modified();
    omesh->Modified();



    vtkSmartPointer<vtkDataSetWriter> wr = vtkSmartPointer<vtkDataSetWriter>::New();
    wr->SetInputData(omesh);
    wr->SetFileTypeToASCII();


    std::string dumpFileName;
    dumpFileName.assign(filename.data());
    dumpFileName.replace(dumpFileName.end()-4,dumpFileName.end(),"_seg.vtk");
    wr->SetFileName(dumpFileName.data());
    wr->Update();
    wr->Write();

}

void MainWindow::dump_info_on_close()
{

    std::string dumpFileName;
    ofstream dumpFile;
    dumpFileName.assign(filename.data());
    dumpFileName.replace(dumpFileName.end()-4,dumpFileName.end(),"_edge.csv");
    dumpFile.open(dumpFileName.data(),ios::trunc|ios::out);
    for(SSP_MAP::iterator it = data->ssp_min.begin(); it != data->ssp_min.end(); it++)
    {
        int i = it->first.first ;
        int j = it->first.second ;

        double imp = it->second.first.second;
        double p = min(data->persistence[i],data->persistence[j]);
        double c =it->second.second.first;
        dumpFile<<imp<<","<<p<<","<<c<<endl;
    }
    dumpFile.close();
    dumpPersistenceStatistics();

    BEGIN_TIME;
    data->finalizeSSP();
    END_TIME;
    DISP_TIME("Finalize ssp : ");

    BEGIN_TIME;
    for(std::map<int,std::set<int> >::iterator it = data->neighbours.begin(); it != data->neighbours.end(); it++)
    {
        std::set<int> s = it->second;
        std::set<int> c = data->children[it->first];
        std::set<int> p = data->candidateSets[it->first];
        std::set<int> result;
        std::set_difference(s.begin(),s.end(),c.begin(),c.end(),std::inserter(result,result.begin()));
        s = result;
        result.clear();
        std::set_difference(s.begin(),s.end(),p.begin(),p.end(),std::inserter(result,result.begin()));
        data->neighbours[it->first] = result;
    }
    END_TIME;
    DISP_TIME("Nbd Redundancy : ");

    BEGIN_TIME;
    dumpInformation();
    END_TIME;
    DISP_TIME("Dump Information : ");

    BEGIN_TIME;
    dumpSegmentedData();
    END_TIME;
    DISP_TIME("Write Mesh : ");

    delete[] data->scalars;


}


