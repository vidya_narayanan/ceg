#include "dataset.h"
//#include "cpputils.h"
#include <assert.h>
#include <algorithm>
#include <queue>
#include <iterator>
#include <fstream>

#define TRIPAIR(x,y,z) make_pair(make_pair(x,y),z)

pPair::pPair(int s, int e, int ee, bool area_wise)
{

    sad = s; ext = e; sur_ext = ee;
    if(ee != -1)
    {
        double v1 = fabs(dataset::getInstance().scalars[e]- dataset::getInstance().scalars[sad]);
        double v2 = fabs(dataset::getInstance().scalars[ee]- dataset::getInstance().scalars[sad]);
        double a1 = dataset::getInstance().regionSize[e];
        double a2 = dataset::getInstance().regionSize[ee];
        bool flip1 = dataset::getInstance().scalars[e] == dataset::getInstance().scalars[ee] ? e > ee : dataset::getInstance().scalars[e] > dataset::getInstance().scalars[ee];
        bool flip2 = (v1*a1 > v2*a2) ?  true : false;
        bool flip = (flip1 && !area_wise) || (flip2 && area_wise );
        if(flip)
        {
            sur_ext = e;
            ext = ee;
        }
    }

    double saddle = s < 0 ? 1 : dataset::getInstance().scalars[sad];
    double extrema = dataset::getInstance().scalars[ext];
    assert(dataset::getInstance().max_region_size > 0);
    double init_area = dataset::getInstance().regionSize[ext];
    double area = init_area;
    //assert(area >=0 && area <=1);
    if(area_wise)
        value = fabs(saddle - extrema)*area;
    else
        value = (extrema-saddle);
    //  value = area;
    assert(value >= 0 && value <=1);
}



double dataset::createArcPersistenceMap(bool area)
{

    while(!pqPers.empty())
        pqPers.pop();

    std::vector<double> thresholds;


    for(SSP_MAP::iterator it = ssp_min.begin(); it != ssp_min.end(); it++)
    {
        int m1 = (it->first).first;
        int m2 = (it->first).second;
        int s  = ((it->second).first).first;
        pPair pr = pPair(s,m1,m2,area);
        pqPers.push(pr);
        thresholds.push_back(pr.value);

    }

    // auto-determine threshold and return it;
    double minimum_threshold = 1e-5;
    double maximum_threshold = 0.6;
    std::sort(thresholds.begin(),thresholds.end(),std::less<double>());
    std::vector<double> threshold_values;

    double max_v = 0;

    for(unsigned int i =1 ; i < thresholds.size(); i++)
    {
        double v0 = thresholds[i-1];
        double v1 = thresholds[i];
        double curr = v1 - v0;

        if(curr > 1.5*max_v)
        {
            max_v = 0;
            if(v0 > minimum_threshold && v0 < maximum_threshold)
                threshold_values.push_back(v0);
        }
        max_v = max(max_v,curr);

    }
    double dyn_threshold =  threshold_values.size() ? threshold_values[0] : minimum_threshold;
    return dyn_threshold;

}
void dataset::growNext(int id, int reg_id)
{


    std::set<int> all_labels = labelListSets[id];
    for(std::set<int>::iterator it = lowerRingSets[id].begin(); it != lowerRingSets[id].end(); it++)
    {
        if(labelListSets[*it].find(reg_id) == labelListSets[*it].end() && ( labelListSets[*it].size()<2))
        {
            for(std::set<int>::iterator it1 = all_labels.begin(); it1 != all_labels.end(); it1++)
            {
                labelListSets[*it].insert(*it1);
            }
        }
    }


}

void  dataset::updateMoments(int reg_id, int px_id)
{
    //m_xyz
    double f = scalars[px_id];
    point<double> pt = extremaPositions[px_id];
    RegionMoments[reg_id].m000 += f;
    RegionMoments[reg_id].m002 += f*(pt.getz()*pt.getz());
    RegionMoments[reg_id].m020 += f*(pt.gety()*pt.gety());
    RegionMoments[reg_id].m200 += f*(pt.getx()*pt.getx());
    RegionMoments[reg_id].m011 += f*(pt.gety()*pt.getz());
    RegionMoments[reg_id].m101 += f*(pt.getx()*pt.getz());
    RegionMoments[reg_id].m110 += f*(pt.getx()*pt.gety());
    RegionMoments[reg_id].m100 += f*pt.getx();
    RegionMoments[reg_id].m010 += f*pt.gety();
    RegionMoments[reg_id].m001 += f*pt.getz();
}




void dataset::growAllRegions(double bf)
{

    boundaryFraction = bf;

    ssp_min.clear();
    std::vector<order> ordervector;
    for(int i = 0; i < numPoints;i++)
    {
        order o;
        o.id = i;
        o.value = scalars[i];
        ordervector.push_back(o);
    }
    std::sort(ordervector.begin(),ordervector.end(),order_greater());
    for(std::vector<order>::iterator it = ordervector.begin(); it != ordervector.end(); it++)
    {
        int id = (*it).id;

        if(labelListSets[id].size()==1)
        {
            int reg_id = *(labelListSets[id].begin());
            growNext(id,reg_id);
        }


    }


    std::map< std::pair<int,int> , int > AdjacencyPairs;
    std::map< int,int> boundarySize;

    for(std::vector<order>::iterator it = ordervector.begin(); it != ordervector.end(); it++)
    {

        int k = (*it).id;
        if(labelListSets[k].size()<2)
            continue;
        if(lowerRingSets[k].size()==0)
            continue;
        assert(labelListSets[k].size()==2);
        for(std::set<int>::iterator it1 =labelListSets[k].begin(); it1 != labelListSets[k].end(); it1++)
        {
            std::set<int>::iterator it2 = it1;
            it2++;
            for(; it2 != labelListSets[k].end(); it2++)
            {

                int i = min(*it1,*it2);
                int j = max(*it1,*it2);


                AdjacencyStrength[make_pair(i,j)] = AdjacencyStrength[make_pair(i,j)]+1;
                boundarySize[i] = boundarySize[i] + 1;
                boundarySize[j] = boundarySize[j] + 1;

                if( AdjacencyPairs[make_pair(i,j)] == 0 || (AdjacencyPairs[make_pair(i,j)] > 0 &&
                                                            scalars[AdjacencyPairs[make_pair(i,j)]-1] <= scalars[k]) )//changed <=
                {
                    AdjacencyPairs[make_pair(i,j)] = k+1;
                    ssp_min[make_pair(i,j)] = make_pair(make_pair(k, 0.0),make_pair(0,0));

                }
                int k_ = lowestSaddle[make_pair(i,j)];
                lowestSaddle[make_pair(i,j)] = (k_==0 || scalars[k] < scalars[k_]) ? k : k_;

            }
        }
    }

    SSP_MAP temp = ssp_min;
    ssp_min.clear();

    cout<<"Pre-process : "<<endl;

    for(int k = 0; k < numPoints; k++)
    {
        if(labelListSets[k].size()==1)
        {
            int reg_id = *(labelListSets[k].begin());
            {
                updateMoments(reg_id,k);
                in_moment[k] = 1;


            }
            regionSize[reg_id] = regionSize[reg_id] + 1;

        }

    }


    for(SSP_MAP::iterator it = temp.begin(); it != temp.end(); it++)
    {
        pair<int,int> key = it->first;
        //        double bnd = AdjacencyStrength[key];
        //        double a1 = regionSize[key.first];
        //        double a2 = regionSize[key.second];
        //        assert(bnd <= a1 && bnd <= a2);
        //        assert(bnd > 0);
        //        double bF = bnd/min(a1,a2);
        int k = ((it->second).first).first;


        //if(bF >= boundaryFraction /*|| pval < simUpto*/)
        {


            ((it->second).second).first = 0;
            ssp_min.insert(make_pair(it->first,it->second));

            neighbours[key.first].insert(key.second);
            neighbours[key.second].insert(key.first);

            assert(extremaSets.find(k)==extremaSets.end());
            vsaddles.insert(k);
            saddleSets[key.first].insert(k);
            saddleSets[key.second].insert(k);

            ssps[make_pair(key.first,k)] = key.second;
            ssps[make_pair(key.second,k)] = key.first;
            persistentPair[k]=-1;
            persistence[k]= 1;
            //cout<< key.first <<" , " << key.second << " : " << k << endl;

        }

    }

    for(std::vector<order>::iterator it = ordervector.begin(); it != ordervector.end(); it++)
    {
        int id= (*it).id;
        if(extremaSets.find(id) == extremaSets.end())
            continue;


        if(saddleSets[id].size()==0)
        {

            std::set<int> lr = lowerRingSets[id];
            double t1 = 0;
            int t2 = -1;
            for(std::set<int>::iterator lit = lr.begin(); lit != lr.end(); lit++)
            {
                if(labelListSets[*lit].size()==0)
                    continue;
                if(scalars[*lit] > t1)
                {
                    t1 = scalars[*lit];
                    t2 = *lit;
                }

            }
            assert(t2!=-1);
            labelListSets[id].clear();
            labelListSets[id] = labelListSets[t2];
            extremaSets.erase(id);

        }

    }

    max_region_size = numPoints;
    //max_distance = 0;
    for(std::vector<order>::iterator it = ordervector.begin(); it != ordervector.end(); it++)
    {

        int id = (*it).id;
        double regsize = regionSize[id];
        regionSize[id] = regsize/max_region_size;
        assert(labelListSets[id].size() <= 2);

    }


    //Pre Process Edges
    cout<<"Pre Process Edges .. ";
    ssptemp = ssp_min;

}

void dataset::finalizeSSP()
{
    //assert(max_distance > 0);
    SSP_MAP temp = ssp_min;
    ssp_min.clear();
    SSP_MAP::iterator iter = temp.begin();

    cout << "SSP Size : "  << temp.size() << endl;
    iter = temp.begin();
    int n = 0;
    while(iter != temp.end())
    {
        n++;
        pair<int,int> key = iter->first;
        int s = ((iter->second).first).first;
        double p = ((iter->second).first).second;

        assert(p <= 1.0);
        int m1 = key.first;
        int m2 = key.second;


        if(persistence[m1]<=simUpto || persistence[m2]<=simUpto || m1==m2 )
        {
            iter++;
            continue;
        }

        pair<double,double> threshold;
        threshold.first = iter->second.second.first;
        threshold.second = iter->second.second.second;



        key.first =  ext_ids[m1]+1;
        key.second = ext_ids[m2]+1;
        assert(vsaddles.find(s) != vsaddles.end() || s==-1);
        s =  sad_ids[s]+1;
        ssp_min.insert(make_pair(key,make_pair(make_pair(s,p),threshold)));
        iter++;
    }

}



void dataset::restoreState()
{

    for(std::map<int,std::set<int> >::iterator it = neighbours.begin(); it != neighbours.end(); it++)
    {
        if(persistence[it->first] <= simUpto)
            continue;
        std::set<int> nbrs = it->second;
        for(std::set<int>::iterator it1 = nbrs.begin(); it1 != nbrs.end(); )
        {
            if(persistence[*it1] <= simUpto)
                nbrs.erase(it1++);
            else
                ++it1;
        }
        it->second = nbrs;
    }

    //purge all data in nbrs, parents, children, adjextrema , adjsaddles,
    for(std::map<int,double>::iterator it = persistence.begin(); it != persistence.end(); it++)
    {
        int id = it->first;
        if(it->second <= simUpto)
        {
            neighbours.erase(id);
            children.erase(id);
            candidateSets.erase(id);
            RegionMoments.erase(id);
            regionSize.erase(id);
        }
    }

}

void dataset::computeAPSD(double threshold)
{
    priority_queue< pPair, vector<pPair>, pPair_greater > arcs;
    std::map<int, std::set<pair<int,int> > > nbrs;
    std::map<pair<int,int>,double> sad_costs;
    std::map<pair<int,int>,bool> usedKeys;
    std::map<int, double> psuedoPersistence;

    for(std::set<int>::iterator it1 = extremaSets.begin(); it1 != extremaSets.end(); it1++)
    {
        std::set<int>::iterator it2 = it1;
        for(; it2 != extremaSets.end(); it2++)
        {
            int i = min(*it1,*it2);
            int j = max(*it1,*it2);
            intermediate[make_pair(i,j)]=-1;

            SSP_MAP::iterator iter = ssptemp.find(make_pair(i,j));
            if( iter != ssptemp.end())
            {

                int s = iter->second.first.first;
                double c = iter->second.first.second;
                arcs.push(pPair(s,i,j));
                nbrs[i].insert(make_pair(s,j));
                nbrs[j].insert(make_pair(s,i));
                costs[make_pair(i,j)] = c;
                sads[make_pair(i,j)] = s;
                sad_costs[make_pair(i,j)] = c;
                arc_saddles[make_pair(i,j)] = -1;
                usedKeys[make_pair(i,j)]=false;

            }
            else
            {
                costs[make_pair(i,j)] = (i==j) ? 0 : DBL_MAX;
                sads[make_pair(i,j)] = -1;
                sad_costs[make_pair(i,j)] =  (i==j) ? 0 : DBL_MAX;;
                arc_saddles[make_pair(i,j)] = -1;
                usedKeys[make_pair(i,j)]=false;
            }
        }
    }

    while(!arcs.empty())
    {
        pPair p = arcs.top();
        arcs.pop();
        int i = p.ext;
        int j = p.sur_ext;
        int s = p.sad;

        pair<int,int> key_ij = make_pair(min(i,j),max(i,j));
        // replaced
        if(sads[key_ij] != s)
            continue;
        assert(arc_saddles[key_ij] == -1 );
        arc_saddles[key_ij] = s;

        double c = p.value;
        double x = costs[key_ij];
        double y = sad_costs[key_ij];

        assert(!usedKeys[key_ij]);
        //once popped, never update
        usedKeys[key_ij]=true;
        // only the first time will be inserted
        psuedoPersistence.insert(make_pair(i,p.value));
        assert(i!=j);
        assert(y<=c);
        assert(c>=x);
        nbrs[i].erase(make_pair(s,j));
        nbrs[j].erase(make_pair(s,i));
        std::set<pair<int,int> > i_nbrs = nbrs[i];

        for(std::set<pair<int,int> >::iterator ji = i_nbrs.begin(); ji != i_nbrs.end(); ji++)
        {
            int s_ = ji->first;
            int j_ = ji->second;

            if(j==j_ )
                continue;


            pair<int,int> key = make_pair(min(j,j_),max(j,j_));
            if(usedKeys[key])
                continue;
            //existing
            pPair p1(s_,j_,i);
            //new
            pPair p_(s_,j_,j);

            assert(p1.value >= c);
            assert(p_.value >= c);
            if(p1.value < c)
            {

                assert(costs[key] < 1);
                continue;
            }
            if(p_.value < c)
            {
                continue;
            }

            if(costs[key] > c)
            {
                assert(intermediate[key]==-1);
                intermediate[key] = i;
                costs[key] = c;
                //rsad[key] = s_;
                assert(sads[make_pair(min(i,j_),max(i,j_))]==s_);
                sads[key] = s_;
                sad_costs[key] = c;
                nbrs[j].insert(make_pair(s_,j_));
                nbrs[j_].insert(make_pair(s_,j));
                arcs.push(p_);

            }
            else
            {
                int s__ = sads[key];
                assert(s__ != -1);

                if(scalars[s_] > scalars[s__] )
                {
                    assert(sads[make_pair(min(i,j_),max(i,j_))]==s_);
                    sads[key] = s_;
                    assert(costs[key]<= c);
                    nbrs[j].insert(make_pair(s_,j_));
                    nbrs[j_].insert(make_pair(s_,j));
                    nbrs[j].erase(make_pair(s__,j_));
                    nbrs[j_].erase(make_pair(s__,j));
                    nbrs[j].erase(make_pair(s__,i));
                    nbrs[j_].erase(make_pair(s__,i));
                    nbrs[i].erase(make_pair(s__,j));
                    nbrs[i].erase(make_pair(s__,j_));
                    sad_costs[key] = c;
                    arcs.push(p_);

                }
            }
        }

    }


    ssp_min.clear();

    for(map<pair<int,int>,double>::iterator it = costs.begin(); it != costs.end(); it++)
    {

        pair<int,int> key = it->first;
        double cost =  it->second;

        if(key.first == key.second)
            continue;

        int     s = arc_saddles[key];
        double minp =  min(persistence[key.first],persistence[key.second]);
        double edge_imp = max(0.0, (minp - cost)/minp);

        assert(s!=-1 || cost > 1 || edge_imp == 0);

        cost = min(cost,minp);
        double m1 = scalars[key.first];
        double m2 = scalars[key.second];
        double s12 = s != -1 ? scalars[s] :-1;
        double p1 = m1 - s12;
        double p2 = m2 - s12;
        assert(p1 >= 0 && p2 >=0);
        assert(minp >= simUpto);
        if(minp <= simUpto)
            continue;

        double hd = 0;
        assert(cost >=0 && cost <= 1);
        assert(ssp_min.find(key) == ssp_min.end());
        if(key.first != key.second && cost <= threshold)
            ssp_min[key]= make_pair(make_pair(s,edge_imp),make_pair(cost,hd));

    }

}

void dataset::getPathKey(pair<int,int> key)
{
    if(intermediate[key]==-1)
        return;
    int i = intermediate[key];
    pair<int,int> pleft = make_pair(min(i,key.first),max(i,key.first));
    pair<int,int> pright = make_pair(min(i,key.second),max(i,key.second));
    getPathKey(pleft);
    cout<<" - "<<i<<" - ";
    getPathKey(pright);

}

void dataset::simplifyExtrema(double area_threshold)
{
    cout<<"Modifying area threshold from: " <<area_threshold<<endl;

    //area_threshold=createArcPersistenceMap(true);
    createArcPersistenceMap(true);
    cout<<" to "<<area_threshold<<endl;

    double area_simp_threshold = area_threshold;
    double curr_value = 0;
    int numExtremaTotal = extremaSets.size();
    cout<<"Initial Num of Extrema: "<<numExtremaTotal<<endl;
    while(!pqPers.empty() && curr_value < area_simp_threshold )
    {
        pPair top = pqPers.top();
        std::pair<int,int> sad_ext = top.getPair() ;
        pqPers.pop();

        int extremaToDelete = sad_ext.second;
        int saddleToDelete = sad_ext.first;
        int survivingExtrema = top.sur_ext;

        curr_value = top.value;


        if(pPair(saddleToDelete,extremaToDelete,survivingExtrema,true).value > top.value)
        {
            pqPers.push(pPair(saddleToDelete,extremaToDelete,survivingExtrema,true));
            continue;
        }


        if( ( criticalPaired[extremaToDelete] ) || criticalPaired[saddleToDelete] || criticalPaired[survivingExtrema])
        {

            continue;
        }


        if(survivingExtrema == -1)
            continue;
        assert(!criticalPaired[survivingExtrema] );

        if(survivingExtrema == extremaToDelete)
        {
            continue;
        }

        assert(saddleSets[extremaToDelete].size() > 0);
        assert(saddleSets[extremaToDelete].find(saddleToDelete) != saddleSets[extremaToDelete].end());
        assert(saddleSets[survivingExtrema].find(saddleToDelete) != saddleSets[survivingExtrema].end());
        // cout << "Merge: " << survivingExtrema << " <---+ "<<saddleToDelete<<" +---" << extremaToDelete << endl;

        std::set<int> saddle_set = saddleSets[survivingExtrema];
        saddle_set.erase(saddleToDelete);
        std::set<int> additional_set = saddleSets[extremaToDelete];
        additional_set.erase(saddleToDelete);




        bool simplified = false;
        if( top.value <= area_simp_threshold )
        {
            regionSize[survivingExtrema] += regionSize[extremaToDelete];
            //regionSize[survivingExtrema] -= AdjacencyStrength[make_pair(min(extremaToDelete,survivingExtrema),max(extremaToDelete,survivingExtrema))];
            RegionMoments[survivingExtrema] = RegionMoments[survivingExtrema] + RegionMoments[extremaToDelete];
            numExtremaTotal--;


            simplified = true;
        }


        if(!simplified)
            continue;

        //Add all of additional_set saddles to survivingExtrema
        for(std::set<int>::iterator it = additional_set.begin(); it != additional_set.end(); it++)
        {
            // is surviving extrema already connected to the region connected by some saddle ?
            int s_ = *it;
            assert(vsaddles.find(s_) != vsaddles.end());
            if(criticalPaired[s_])
                continue;
            int r_ = ssps[make_pair(extremaToDelete, s_)];
            if(r_ == -1 || criticalPaired[r_]/*extremaLives[r_]<=0*/)
                continue;
            std::set<int> t = neighbours[survivingExtrema];
            t.erase(extremaToDelete);
            if(t.find(r_) == t.end())
            {
                saddleSets[survivingExtrema].insert(s_);
                ssps[make_pair(survivingExtrema,s_)]=r_;
                ssps[make_pair(r_,s_)]=survivingExtrema;
                pqPers.push(pPair(s_,survivingExtrema,r_,true));
                pqPers.push(pPair(s_,r_,survivingExtrema,true));
                assert(ssp_min.find(make_pair(min(r_,survivingExtrema),max(r_,survivingExtrema))) == ssp_min.end());
                assert(ssptemp.find(make_pair(min(r_,survivingExtrema),max(r_,survivingExtrema))) == ssptemp.end());

                neighbours[survivingExtrema].insert(r_);
                neighbours[r_].insert(survivingExtrema);
                assert(saddleSets[r_].find(s_) != saddleSets[r_].end());

                ssp_min.insert(make_pair(make_pair(min(r_,survivingExtrema),max(r_,survivingExtrema)),
                                         make_pair(make_pair(s_,0),make_pair(0,0))));

                int i = min(r_,survivingExtrema);
                int j = max(r_,survivingExtrema);
                int k_ = lowestSaddle[make_pair(i,j)];
                int k = lowestSaddle[make_pair(min(r_,extremaToDelete),max(r_,extremaToDelete))];
                //assert(k!=0);
                lowestSaddle[make_pair(i,j)] = (k_==0 || scalars[k] < scalars[k_]) ? s_ : k_;

                AdjacencyStrength[make_pair(min(r_,survivingExtrema),max(r_,survivingExtrema))] +=
                        AdjacencyStrength[make_pair(min(r_,extremaToDelete),max(r_,extremaToDelete))];
            }
            else
            {
                SSP_MAP::iterator iter = ssp_min.find(make_pair(min(r_,survivingExtrema),max(r_,survivingExtrema)));
                assert(iter != ssp_min.end());
                int s__ = ((iter->second).first).first;
                if(scalars[s_] > scalars[s__]  )
                {

                    pPair p(s_,survivingExtrema,r_,true);
                    assert(p.value >= top.value);

                    saddleSets[survivingExtrema].insert(s_);
                    saddleSets[r_].insert(s_);
                    saddleSets[survivingExtrema].erase(s__);
                    saddleSets[r_].erase(s__);

                    ssps[make_pair(survivingExtrema,s_)]=r_;
                    ssps[make_pair(r_,s_)]=survivingExtrema;
                    ssps[make_pair(survivingExtrema,s__)]= -1;
                    ssps[make_pair(r_,s__)]= -1;
                    pqPers.push(pPair(s_,survivingExtrema,r_,true));
                    ssp_min.erase(iter);
                    ssp_min[make_pair(min(r_,survivingExtrema),max(r_,survivingExtrema))]=
                            make_pair(make_pair(s_,top.value),make_pair(0,0));
                    neighbours[survivingExtrema].insert(r_);
                    neighbours[r_].insert(survivingExtrema);

                    int i = min(r_,survivingExtrema);
                    int j = max(r_,survivingExtrema);
                    int k_ = lowestSaddle[make_pair(i,j)];
                    int k = lowestSaddle[make_pair(min(r_,extremaToDelete),max(r_,extremaToDelete))];
                    //assert(k!=0);
                    lowestSaddle[make_pair(i,j)] = (k_==0 || scalars[k] < scalars[k_]) ? s_ : k_;

                    AdjacencyStrength[make_pair(min(r_,survivingExtrema),max(r_,survivingExtrema))] +=
                            AdjacencyStrength[make_pair(min(r_,extremaToDelete),max(r_,extremaToDelete))];

                }
            }
        }



        persistence[extremaToDelete] = 0;//top.value;
        persistence[saddleToDelete] = 0;//top.value;
        persistentPair[extremaToDelete] = saddleToDelete;
        persistentPair[saddleToDelete] = extremaToDelete;
        persistentParent[extremaToDelete] = survivingExtrema;
        criticalPaired[extremaToDelete] = true;
        criticalPaired[saddleToDelete] = true;

    }



    for(std::set<int>::iterator it = extremaSets.begin(); it != extremaSets.end(); )
    {
        if(persistence[*it] <= area_simp_threshold)
            extremaSets.erase(it++);
        else
            ++it;
    }
    for(SSP_MAP::iterator it  = ssp_min.begin(); it != ssp_min.end();)
    {
        double minp = min(persistence[it->first.first],persistence[it->first.second]);
        if(minp <= area_simp_threshold)
            ssp_min.erase(it++);
        else{
            it->second.first.second=0; //Added to make the correct extremum graph
            ++it;
        }
    }


    cout<<"Total Extrema(Area-Based Simp): " <<numExtremaTotal<<endl;
}


void dataset::computePersistence(int numExtremaRemaining)
{


    double dynSimUpto =  createArcPersistenceMap(false);

    cout<<"Modifying Simupto from "<<simUpto <<" to "<<dynSimUpto<<endl;
    simUpto  = dynSimUpto;
    ssptemp = ssp_min;

    int numExtremaTotal = extremaSets.size();
    double actualSimplifiedUpto = 0;
    std::set<int> orb;
    while(!pqPers.empty())
    {
        pPair top = pqPers.top();
        std::pair<int,int> sad_ext = top.getPair() ;
        pqPers.pop();

        int extremaToDelete = sad_ext.second;
        int saddleToDelete = sad_ext.first;
        int survivingExtrema = top.sur_ext;



        if(pPair(saddleToDelete,extremaToDelete,survivingExtrema).value > top.value)
        {
            pqPers.push(pPair(saddleToDelete,extremaToDelete,survivingExtrema));
            continue;
        }


        if( ( criticalPaired[extremaToDelete] ) || criticalPaired[saddleToDelete] || criticalPaired[survivingExtrema])
        {
            continue;
        }

        //        if(candidateSets[extremaToDelete].find(survivingExtrema) != candidateSets[extremaToDelete].end())
        //            continue;
        if(survivingExtrema == -1)
            continue;
        assert(!criticalPaired[survivingExtrema]/* && persistentParent[survivingExtrema]==0*/ );

        if(survivingExtrema == extremaToDelete)
        {

            orb.insert(survivingExtrema);
            continue;
        }

        assert(saddleSets[extremaToDelete].size() > 0);
        assert(saddleSets[extremaToDelete].find(saddleToDelete) != saddleSets[extremaToDelete].end());
        assert(saddleSets[survivingExtrema].find(saddleToDelete) != saddleSets[survivingExtrema].end());
        // cout << "Merge: " << survivingExtrema << " <---+ "<<saddleToDelete<<" +---" << extremaToDelete << endl;
        //reverse path,  handle saddles
        //identify orbits
        std::set<int> saddle_set = saddleSets[survivingExtrema];
        saddle_set.erase(saddleToDelete);
        std::set<int> additional_set = saddleSets[extremaToDelete];
        additional_set.erase(saddleToDelete);

        //bool firstrun = (extremaLives[extremaToDelete] == extremaLife[extremaToDelete]);


        bool simplified = false;
        if( (top.value <= simUpto || numExtremaTotal > numExtremaRemaining ))
        {
            regionSize[survivingExtrema] += regionSize[extremaToDelete];
            regionSize[survivingExtrema] -= AdjacencyStrength[make_pair(min(extremaToDelete,survivingExtrema),max(extremaToDelete,survivingExtrema))];
            RegionMoments[survivingExtrema] = RegionMoments[survivingExtrema] + RegionMoments[extremaToDelete];
            numExtremaTotal--;
            actualSimplifiedUpto = top.value;
            //simUpto = max(simUpto,top.value);
            simplified = true;
        }

        //Add all of additional_set saddles to survivingExtrema
        for(std::set<int>::iterator it = additional_set.begin(); it != additional_set.end(); it++)
        {
            // is surviving extrema already connected to the region connected by some saddle ?
            int s_ = *it;
            assert(vsaddles.find(s_) != vsaddles.end());
            if(criticalPaired[s_])
                continue;
            int r_ = ssps[make_pair(extremaToDelete, s_)];
            if(r_ == -1 || criticalPaired[r_]/*extremaLives[r_]<=0*/)
                continue;
            std::set<int> t = neighbours[survivingExtrema];
            t.erase(extremaToDelete);
            if(t.find(r_) == t.end())
            {
                saddleSets[survivingExtrema].insert(s_);
                ssps[make_pair(survivingExtrema,s_)]=r_;
                ssps[make_pair(r_,s_)]=survivingExtrema;
                pqPers.push(pPair(s_,survivingExtrema,r_));
                pqPers.push(pPair(s_,r_,survivingExtrema));
                assert(ssp_min.find(make_pair(min(r_,survivingExtrema),max(r_,survivingExtrema))) == ssp_min.end());
                assert(ssptemp.find(make_pair(min(r_,survivingExtrema),max(r_,survivingExtrema))) == ssptemp.end());

                ssp_min.insert(make_pair(make_pair(min(r_,survivingExtrema),max(r_,survivingExtrema)),
                                         make_pair(make_pair(s_,top.value),make_pair(0,0))));
                neighbours[survivingExtrema].insert(r_);
                neighbours[r_].insert(survivingExtrema);
                assert(saddleSets[r_].find(s_) != saddleSets[r_].end());
                if(simplified)
                {
                    ssptemp.insert(make_pair(make_pair(min(r_,survivingExtrema),max(r_,survivingExtrema)),
                                             make_pair(make_pair(s_,0),make_pair(0,0))));

                    int i = min(r_,survivingExtrema);
                    int j = max(r_,survivingExtrema);
                    int k_ = lowestSaddle[make_pair(i,j)];
                    int k = lowestSaddle[make_pair(min(r_,extremaToDelete),max(r_,extremaToDelete))];
                    //assert(k!=0);
                    lowestSaddle[make_pair(i,j)] = (k_==0 || scalars[k] < scalars[k_]) ? s_ : k_;



                }
                AdjacencyStrength[make_pair(min(r_,survivingExtrema),max(r_,survivingExtrema))] +=
                        AdjacencyStrength[make_pair(min(r_,extremaToDelete),max(r_,extremaToDelete))];
            }
            else
            {
                SSP_MAP::iterator iter = ssp_min.find(make_pair(min(r_,survivingExtrema),max(r_,survivingExtrema)));
                assert(iter != ssp_min.end());
                int s__ = ((iter->second).first).first;
                if(scalars[s_] > scalars[s__]  )
                {

                    pPair p(s_,survivingExtrema,r_);
                    assert(p.value >= top.value);

                    saddleSets[survivingExtrema].insert(s_);
                    saddleSets[r_].insert(s_);
                    saddleSets[survivingExtrema].erase(s__);
                    saddleSets[r_].erase(s__);

                    ssps[make_pair(survivingExtrema,s_)]=r_;
                    ssps[make_pair(r_,s_)]=survivingExtrema;
                    ssps[make_pair(survivingExtrema,s__)]= -1;
                    ssps[make_pair(r_,s__)]= -1;
                    pqPers.push(pPair(s_,survivingExtrema,r_));
                    ssp_min.erase(iter);
                    ssp_min[make_pair(min(r_,survivingExtrema),max(r_,survivingExtrema))]=
                            make_pair(make_pair(s_,top.value),make_pair(0,0));
                    neighbours[survivingExtrema].insert(r_);
                    neighbours[r_].insert(survivingExtrema);

                    if(simplified)
                    {

                        ssptemp[make_pair(min(r_,survivingExtrema),max(r_,survivingExtrema))]=
                                make_pair(make_pair(s_,/*top.value*/0),make_pair(0,0));

                        int i = min(r_,survivingExtrema);
                        int j = max(r_,survivingExtrema);
                        int k_ = lowestSaddle[make_pair(i,j)];
                        int k = lowestSaddle[make_pair(min(r_,extremaToDelete),max(r_,extremaToDelete))];
                        //assert(k!=0);
                        lowestSaddle[make_pair(i,j)] = (k_==0 || scalars[k] < scalars[k_]) ? s_ : k_;



                    }
                    AdjacencyStrength[make_pair(min(r_,survivingExtrema),max(r_,survivingExtrema))] +=
                            AdjacencyStrength[make_pair(min(r_,extremaToDelete),max(r_,extremaToDelete))];

                }
            }
        }

        persistence[extremaToDelete] = top.value;
        persistence[saddleToDelete] = top.value;
        persistentPair[extremaToDelete] = saddleToDelete;
        persistentPair[saddleToDelete] = extremaToDelete;
        persistentParent[extremaToDelete] = survivingExtrema;
        criticalPaired[extremaToDelete] = true;
        criticalPaired[saddleToDelete] = true;
        candidateSets[extremaToDelete].insert(survivingExtrema);


    }
    std::set<int> newsaddles;
    most_persistent_saddle = -1;

    for(SSP_MAP::iterator it = ssptemp.begin(); it != ssptemp.end(); )
    {
        int i = it->first.first;
        int j = it->first.second;
        double minp = min(persistence[i],persistence[j]);
        if(minp <= simUpto)
            ssptemp.erase(it++);
        else{
            newsaddles.insert(it->second.first.first);
            if(persistentPair[it->second.first.first]==-1 )
            {
                if(most_persistent_saddle==-1 || scalars[it->second.first.first] < scalars[most_persistent_saddle])
                    most_persistent_saddle = it->second.first.first;
            }
            ++it;
        }

    }
    vsaddles = newsaddles;

    cout<<"Changing Simplification Threshold from : " << simUpto << " To: "<<actualSimplifiedUpto << endl;

    simUpto = actualSimplifiedUpto;
    for(std::set<int>::iterator it = extremaSets.begin(); it != extremaSets.end(); )
    {
        if(persistence[*it] <= simUpto)
            extremaSets.erase(it++);
        else
        {
            ++it;
        }
    }
    cout<<"Total Extrema: " <<numExtremaTotal<<endl;

}

