# README #

This code constructs the complete extremum graph for a scalar
field provided as a .vtk file ( structured points, polydata, unstructured grid)

The complete extremum graph records importance of all extrema and the importance
of all pairs of edges possible between these extrema.
This is based on local function perturbation necessary to eliminate extrema and
introduce shared saddles between their descending or ascending manifolds

###  Running ###
* ./ceg  file  threshold  type  array


    file:  file name of a structured or unstructured data in vtk format

    threshold :    simplification threshold between 0 and 1 (default is 1 i.e complete simplification)

    type: 1 indicates maximum graph and 0 minimum graph (default is 0)

    array: id or name of the array in the file (default is  0)

### Requires ###

* VTK 6.1
* QT 5 

