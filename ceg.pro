#-------------------------------------------------
#
# Project created by QtCreator 2013-08-23T11:14:11
#
#-------------------------------------------------

QT       += core gui
QT       += widgets

TARGET = ceg
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    dataset.cpp \
    vtkutils.cpp \
    cpputils.cpp

HEADERS  += mainwindow.h \
    dataset.h \
    vtkinc.h \
    vtkutils.h \
    cpputils.h

FORMS    += mainwindow.ui

INCLUDEPATH += . \
/usr/local/include/vtk-6.1/ \
#/usr/local/include/eigen3 \


DEPENDPATH+= . \
/usr/local/lib \
/usr/local/lib/ \

unix:!macx:!symbian: LIBS += -L /usr/local/lib/ -lvtkRenderingCore-6.1 -lvtkGUISupportQt-6.1  -lvtkGUISupportQtOpenGL-6.1 -lvtkCommonCore-6.1  -lvtkFiltersCore-6.1 -lvtkFiltersExtraction-6.1 -lvtkRenderingOpenGL-6.1  -lvtkIOCore-6.1 -lvtkViewsCore-6.1 -lvtkImagingCore-6.1 -lvtkImagingHybrid-6.1 -lvtkFiltersHybrid-6.1
unix:!macx:!symbian: LIBS += -L /usr/local/lib/ -lvtkRenderingOpenGL-6.1  -lvtkInteractionWidgets-6.1 -lvtkInteractionStyle-6.1  -lvtkRenderingFreeType-6.1 -lvtkFiltersTexture-6.1 -lvtkIOExport-6.1 -lgomp -fopenmp
unix:!macx:!symbian: LIBS += -L /usr/local/lib/ -lvtkCommonDataModel-6.1 -lvtkCommonExecutionModel-6.1 -lvtkFiltersSources-6.1 -lvtkCommonMisc-6.1 -lvtkInfovisCore-6.1 -lvtkInfovisLayout-6.1  -lvtkIOInfovis-6.1 -lvtkViewsInfovis-6.1 -lvtkIOImage-6.1 -lvtkIOLegacy-6.1
unix:!macx:!symbian: LIBS += -L /usr/local/lib/ -lvtkFiltersGeneral-6.1 -lvtkFiltersGeneric-6.1 -lvtkFiltersGeometry-6.1 -lvtkFiltersExtraction-6.1 -lvtkFiltersFlowPaths-6.1 -lvtkFiltersModeling-6.1
unix:!macx:!symbian: LIBS += -L /usr/local/lib/ -lvtkCommonComputationalGeometry-6.1
#-lvtkGraphics -lvtkWidgets -lvtkHybrid  -lvtkViews -lvtkInfovis -lvtkCharts -lgomp -fopenmp

#unix:!macx:!symbian: LIBS += -L./libs/ -lnl
#unix:!macx:!symbian: LIBS += -L./libs/ -lsuperlu_4.3
#unix:!macx:!symbian: LIBS += -L./libs/ -lblas

CCFLAG += -std=c++11


OTHER_FILES += \
    notes.txt
